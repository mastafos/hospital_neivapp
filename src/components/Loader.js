import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Spinner } from 'native-base';
export default class Loader extends Component {
  render() {
    return (
      <View style={styles.loaderContainer}>
          <Spinner />
      </View>
    );
  }
}
const styles = StyleSheet.create({
    loaderContainer:{
        flex:1,
        justifyContent:'center'
    }
});
