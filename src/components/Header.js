import 'react-native-gesture-handler';
import React, {useState} from 'react';
import { View, Text, StyleSheet, TouchableHighlight, TouchableWithoutFeedback } from 'react-native';
import {Icon, Fab } from 'native-base';
import {HeaderInfo} from '../utils/headerConst';

const Header = () => {
    // console.log('userInfoSesion: ', userInfoSesion);
    let {firstName,lastName} = HeaderInfo[0];
    const [onFAB, setOnFAB] = useState(false);
    const [cerrarSesion, setCerrarSesion] = useState(false);
    const closeSesionHandle = (e)=>{
        console.log('presiono cerrar sesion: ', e);
        HeaderInfo.shift(); 
    }
    const listener = ()=>{
        console.log('listener12');
    }
    return (
        <TouchableWithoutFeedback onPress={listener}
        >
            <View style={styles.container}>
                <Text style={styles.userInfo}>{firstName+' '+lastName}</Text>
                <Fab
                    active={onFAB}
                    direction="down"
                    style={styles.imageContainer}
                    position="topRight"
                    containerStyle={{marginTop:-25, marginRight:-15}}
                    onPress={() => setOnFAB(!onFAB)}>
                    <Icon name="share" />
                    <TouchableHighlight style={{ backgroundColor: '#34A34F' }} onPress={closeSesionHandle}>
                        <Icon name='session' />
                    </TouchableHighlight>
                </Fab>
            </View>
        </TouchableWithoutFeedback>
    );
};
const styles = StyleSheet.create({
    container:{
        flex: 1,
        // backgroundColor:'red',
        paddingEnd:40,
        flexDirection:'row',
        justifyContent:'center',
        paddingVertical:'8%'
    },
    userInfo:{
        paddingHorizontal: 20,
        fontWeight:'bold',
        fontSize:18
    },
    imageContainer:{
        backgroundColor:'#4f5bff',
        width:40,
        height:40,
        // marginTop:  20

    }
});
export default Header;