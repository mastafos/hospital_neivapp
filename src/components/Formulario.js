import React, {useState} from 'react'
import { View, StyleSheet, TouchableHighlight, Text, Alert} from 'react-native'
import {TextInput, Headline} from 'react-native-paper'

const Formulario = ({submitData, gettingData}) => {
    const [dataFormulary, setDataFormulary] = useState({});

    const showAlert = () =>{
        Alert.alert(
            'Aviso',
            'Todos los campos son requeridos',
            [
                {text: 'OK', onPress: () => {console.log('OK Pressed'); setDataFormulary({})}},
            ]
        );
    }

    const handleSubmit = ()=>{
        console.log('dataFormulary: ', dataFormulary);
        console.log('data: ', data);
        console.log('submitData.data.length: ', submitData.data.length);
        console.log('Object.keys(dataFormulary).length: ', Object.keys(dataFormulary).length);
        let dataFormLong = !!dataFormulary ? Object.keys(dataFormulary).length : {};

        if(dataFormLong !== submitData.data.length){
            return(
                showAlert()
            )
        }

        Object.keys(dataFormulary).forEach((key) =>{
            if (!dataFormulary[key]) {
                return(
                    showAlert()
                )
            }
            else{
                gettingData(dataFormulary);
                setDataFormulary({});
            }
        });
    }
    const handleOnChange = (event) => {
        setDataFormulary({
            ...dataFormulary,
            [event._dispatchInstances.memoizedProps.name]: event.nativeEvent.text
        })
    }
            
    const {data} = submitData;      
    return(
        <View style={styles.container}>
            <Headline style={styles.title}>Formulario {submitData.view ? submitData.view : ''}</Headline>
            {   data instanceof Object ?
                data.map((item, i) => {
                    let key = i+1;
                    let label = item.value;
                    let labelCapitalize = label.charAt(0).toUpperCase() + label.slice(1);
                    return(
                        <TextInput 
                            key={key}
                            label={labelCapitalize}
                            placreholder={item.placeholder}
                            name={item.value}
                            onChange={handleOnChange}
                        />
                    )
                })
                : <Text> No es un Objeto</Text>
            }
            <View style={styles.btnContainer}>
                <TouchableHighlight 
                    style={styles.btnStyle}
                    onPress={()=>handleSubmit()}
                >
                    <Text style={styles.btnText}>Continuar</Text>
                </TouchableHighlight>
                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        marginTop:20,
        marginHorizontal: '2.5%',
        justifyContent:'center'
    },
    title:{
        textAlign: 'center',
        marginTop: 20,
        marginBottom: 30,
        fontSize: 30
    },
    btnContainer:{
        height: 30,
        textAlign: 'center',
        paddingHorizontal:'15%',
        marginTop: 25
    },
    btnStyle:{
        backgroundColor: 'linear-gradient(to right, rgba(128,82,255,1) 0%, rgba(151,64,231,1) 100%)',
        flex: 1,
        justifyContent:'center',
        borderRadius: 10,
        borderColor: 'gray',
        borderWidth: 1
    },
    btnText:{
        textAlign:'center',
        color: 'white'
    }

})

export default Formulario;