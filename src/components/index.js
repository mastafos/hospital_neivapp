import  React,{useState} from 'react'
import {Keyboard, View, StyleSheet, TouchableWithoutFeedback, Alert} from 'react-native'
import Formulary from '../../components/Formulary'
import axios from 'axios'

const CreateUserView = () =>{
const [usuario] = useState({
        data:[
            {   
                id:1,
                value:'name',
                type:'default',
                placeholder:'Juan'
            },
            {
                id:2,
                value:'job',
                type:'default',
                placeholder:'Developer'
            }
        ],
        view: 'crear usuario'
    })
    const [deleteInfo, setDeleteInfo] = useState(false);

    const gettingData = async (inGetting) =>{
        console.log('trajo la respuesta en inGetting: ', inGetting);
        let url= 'https://reqres.in/api/users'
        let toSend = {
            url: url, 
            method: 'post',
            data: inGetting
        };
        console.log('ttoSend: ', toSend);
        await axios(toSend)
        .then( response => {
            console.log('responseLOL: ', response.data, response.status)
            let id = 'Se creó correctamente el usuario con id:'+ response.data.id;
            showAlert(id)
        })
        .catch(error => {
            console.log('error :', error)
            showAlert('Error al crear usuario, desea reintentar?')
        })
    }

    const showAlert = (e) =>{
        Alert.alert(  
            'Aviso',  
            e,
            [
                {text: 'OK', onPress: () => setDeleteInfo(true)},  
            ]  
        );
    }

    const withOutKeyboardFuntion = () =>{
        Keyboard.dismiss();
    }

    return(
        <TouchableWithoutFeedback onPress={withOutKeyboardFuntion}>
            <View style={styles.container}>
                <Formulary submitData={usuario} gettingData={gettingData} deleteInfo={deleteInfo}/>

            </View>
        </TouchableWithoutFeedback>
    )
}
const styles = StyleSheet.create({
    container:{
        flex: 1,
        marginHorizontal: '2%'
    }
});
export default CreateUserView;