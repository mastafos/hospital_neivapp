import React from 'react';
import { View, Text, Button } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
// import AdministradorViewManager from './views/administrador/AdministradorViewManager';
import AdminHomeView from './views/administrador/AdministradorHomeView';
import AdministradorNewUserView from './views/administrador/AdministradorNewUserView';
import AdminUpdateView from './views/administrador/AdministradorUpdateView';
import Login from './views/Login';
import Header from './components/Header';


const ViewRoutes = () =>{
    const Stack = createStackNavigator();

    return(
        <Stack.Navigator initialRouteName="Login">
            <Stack.Screen name="Login" component={Login} options={{title:''}}/>
            <Stack.Screen name="AdminHome" component={AdminHomeView} 
                options={{
                    title: '',
                    headerRight: () => (<Header />),
                }}
            />
            <Stack.Screen name="NewUser" component={AdministradorNewUserView}
                options={ { title: '', headerRight: () => (<Header />) } }
            />
            <Stack.Screen name="UpdateUser" component={AdminUpdateView}
                options={ { title: '', headerRight: () => (<Header />) } }
            />
        </Stack.Navigator>
    )
}
export default ViewRoutes;