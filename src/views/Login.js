import 'react-native-gesture-handler';
import React,{useState} from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback, Keyboard, TouchableHighlight } from 'react-native';
import { Form, Item, Input, Label } from 'native-base';
import Loader from '../components/Loader';
// import Header from './../components/Header';
import {HeaderInfo} from '../utils/headerConst';

const Login = ({ navigation }) => {
  const [turnOnLoader, setTurnOnLoader]= useState(false);
  const [idUser, setidUser] = useState('');
  const [passwordUser, setpasswordUser] = useState('');
  const [idUserValidation, setidUserValidation] = useState(false);
  const [passwordUserValidation, setpasswordUserValidation] = useState(false);
  const [trySubmit, settrySubmit] = useState(false);
  const withOutKeyboardFuntion = () =>{
    Keyboard.dismiss();
  }

  const rolSwitch= (response)=>{
    let{data} =response;
    console.log('El pinche data: ', data);
    HeaderInfo.push(data);
    // <Header userInfoSesion={data}/>
    switch (data.rol) {
      case 1:
        console.log('Administrador');
        navigation.navigate('AdminHome', response);
        break;
      case 2 :
        console.log('Doctor');
        // navigation.navigate('RouteName', { /* params go here */ })
        break;
      case 3 :
        console.log('Paciente');
        // navigation.navigate('RouteName', { /* params go here */ })
        break;
      case 4 :
        console.log('Laboratorista');
        // navigation.navigate('RouteName', { /* params go here */ })
        break;
      default:
        console.log('Error');
        // navigation.navigate('RouteName', { /* params go here */ })
    }
  }

  const handleSubmit = ()=>{
    settrySubmit(true);
    if(idUser === ''){
      console.log('idUser vacio');
      setidUserValidation(true);
    }else{ setidUserValidation(false);}
    if(passwordUser === ''){
      console.log('algunos de los campos vacios');
      setpasswordUserValidation(true);
    }else {setpasswordUserValidation(false);}
    console.log('idUser: ', idUser, ', passwordUser: ', passwordUser);
    if(idUser === '' || passwordUser === '') return
    setTurnOnLoader(true);
    setTimeout(() => {
      console.log('submit 4 segundos despues!');
      setTurnOnLoader(false);
      const responseService = {
        status:200,
        data:{
          rol: 1,
          firstName: 'Jesus',
          secondName: 'Alexis',
          lastName: 'Canon',
          secondLastName: 'Andrade'
        }
      };
      rolSwitch(responseService);
    }, 1000);
  }

  return (
    <TouchableWithoutFeedback onPress={withOutKeyboardFuntion}>
      {turnOnLoader? <Loader/> : 
      <View style={styles.container}>
        <Text style={styles.initialText}> Inicio de Sesión</Text>
        <Form style={styles.form}>
          <Item floatingLabel style={styles.input} error={trySubmit && idUserValidation} success={trySubmit && !idUserValidation}>
            <Label>Número de cédula</Label>
            <Input
              onChangeText={(e)=>{setidUser(e)}}
              keyboardType='numeric'
              value={idUser}
            />
          </Item>
          {idUserValidation  ? <Text style={styles.alert}>Campo requerido</Text> : null}
          <Item floatingLabel last style={styles.input} error={trySubmit && passwordUserValidation} success={trySubmit && !passwordUserValidation}>
            <Label>Contraseña</Label>
            <Input 
              onChangeText={(e)=>{setpasswordUser(e)}}
              keyboardType='visible-password'
              value={passwordUser}
            />
          </Item>
          {passwordUserValidation ? <Text style={styles.alert}>Campo requerido</Text> : null}
          <View style={styles.btnContainer}>
              <TouchableHighlight 
                  style={styles.btnStyle}
                  onPress={()=>handleSubmit()}
              >
                  <Text style={styles.btnText}>Continuar</Text>
              </TouchableHighlight>
              
          </View>
        </Form>
      </View>
      }
    </TouchableWithoutFeedback>
  );
};
const styles = StyleSheet.create({
  container:{
    flex: 1,
    paddingTop:'10%'
  },
  form:{
    paddingHorizontal:'10%'
  },
  initialText:{
    fontWeight:'bold',
    fontSize: 30,
    textAlign:'center'
  },
  input:{
    paddingBottom: 10
  },
  btnContainer:{
    height: 30,
    textAlign: 'center',
    paddingHorizontal:'15%',
    marginTop: 40
  },
  btnStyle:{
    backgroundColor: 'linear-gradient(to right, rgba(128,82,255,1) 0%, rgba(151,64,231,1) 100%)',
    flex: 1,
    justifyContent:'center',
    borderRadius: 10,
    borderColor: 'gray',
    borderWidth: 1
  },
  btnText:{
    textAlign:'center',
    color: 'white'
  },
  alert:{
    color:'red',
    marginHorizontal:'5%'
  }
});
export default Login;