import 'react-native-gesture-handler';
import React,{useState} from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback, Keyboard, TouchableHighlight, Alert } from 'react-native';
import { Item, Input, Label, Content, Picker, Form, DatePicker } from 'native-base';
import Loader from '../../components/Loader';

const AdministradorNewUserView = ({ navigation }) => {
  const [turnOnLoader, setTurnOnLoader]= useState(false);
  const [trySubmit, settrySubmit] = useState(false);
  const [validationPassword, setValidationPassword] = useState(false);
  const [_isEmpty, setIsEmpty] = useState(false);
  // Información del formulario
  const [typeDocument, settypeDocument] = useState('CC');
  const [idUser, setidUser] = useState('');
  const [nameNewUser ,setNameNewUser] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [fechaNacimiento, setfechaNacimiento] = useState( new Date() );
  const [sexo, setSexo] = useState('');
  const [tipoSangre,setTipoSangre] = useState('');
  const [estadoCivil, setEstadoCivil] = useState('');
  const [eps, setEps] = useState('');
  //Datos de contacto
  const [relacionPersonaContacto, setrelacionPersonaContacto] = useState('');
  const [nombreContacto, setNombreContacto] = useState('');
  const [apellidoContacto, setApellidoContacto] = useState('');
  const [telefonoContacto, setTelefonoContacto] = useState('');
  //Datos ubicación
  const [telefonoCasa, setTelefonoCasa] = useState('');
  const [celular, setCelular] = useState('');
  const [telefonoOficina, setTelefonoOficina] = useState('');
  const [direccion, setDireccion] = useState('');
  const [ciudad, setCiudad] = useState('');
  //Información de sesión
  const [profesion, setProfesion] = useState('');
  const [rolUsuario, setRolUsuario] = useState('');
  const [contrasena, setContrasena] = useState('');
  const [confirmContrasena, setConfirmContrasena] = useState('');

  const newUserData = {
    ndi: typeDocument,
    ndi_value: idUser,
    given: nameNewUser,
    family: lastName,
    email: email,
    birthDate: fechaNacimiento,
    gender: sexo,
    bloodtype:tipoSangre,
    maritalStatus: estadoCivil,
    managingOrganization: eps,
    relationship: relacionPersonaContacto,
    givenc: nombreContacto,
    familyc: apellidoContacto,
    telc: telefonoContacto,
    telhome: telefonoCasa,
    telmobile: celular,
    telwork: telefonoOficina,
    line: direccion,
    city: ciudad,
    practitionerRole: profesion,
    roles: rolUsuario,
    password: contrasena
  };

  const showNewUserRegistered = () =>{
    Alert.alert(
      "",
      "Se ha creado satisfactoriamente el nuevo usuario",
      [
        {
          text: "Ok",
          onPress: () => navigation.navigate('AdminHome')
        }
      ],
      { cancelable: false }
    );
  }
  const callingService = () =>{
    setTurnOnLoader(true);
    // let url = 'https://localhost:4000/admin/newUser';
    // let data = newUserData;
    // const response = await axios.get(url)
    setTimeout(() => {
      console.log('submit 4 segundos despues!');
      setTurnOnLoader(false);
      const responseService = {
        status:200,
        data:newUserData
      };
      showNewUserRegistered();
    }, 1000);
  }

  //Realiza la validacion de que sea Ok la validacion de contraseña. si es ok muestra el popUp y redirige a la pantalla anterior.
  const passwordValidationFunction = () => {
    let isOkPassword = false;
    if(contrasena.length ===0 && confirmContrasena.length===0 || contrasena !== confirmContrasena ) {
      console.log('Entró al condicional')
      isOkPassword = true;
    }else{
      console.log('No entró al condicional de passwordValidation')
      isOkPassword=false;
    }
    if(isOkPassword){
      return
    }else return callingService();
  }

  const validationIsRequired = () =>{
    Alert.alert(
      "Hola,",
      "por favor ingresa toda la informacion requerida",
      [
        {
          text: "Ok",
          onPress: () => console.log("OK Pressed")
        }
      ],
      { cancelable: false }
    );
  }

  const withOutKeyboardFuntion = () =>{
    Keyboard.dismiss();
  }

  //Manejan los cambios de estado de los pickers (selects)
  const handleTypeDocument = (value) =>{
    console.log('value: ', value);
    settypeDocument(value);
  }
  const handleSeRolUsuario = (value)=>{
    setRolUsuario(value);
  }
  const handleSetSexo = (value)=>{
    setSexo(value);
  }
  const handleSetTipoSangre = (value)=>{
    setTipoSangre(value);
  }
  const handleSetEstadoCivil = (value)=>{
    setEstadoCivil(value);
  }

  const sethandleDatebirthday = (date) =>{
    console.log('dateNew: ', date);
    setfechaNacimiento(date);
  }

  //Maneja las vlsaidaciones despues del click en "Agregar"
  const handleSubmit = () => {
    settrySubmit(true);
    
    const esVacio = Object.keys(newUserData).filter((key) => {
      console.log('newUserData[key] === :', newUserData[key] === '');
      return newUserData[key] === '';
    })

    console.log('esVacio1: ', esVacio.length);



    if(esVacio.length){
       validationIsRequired();
       return
    }else{
      return passwordValidationFunction();
    }
  }

  return (
    <TouchableWithoutFeedback onPress={withOutKeyboardFuntion}>
      {turnOnLoader? <Loader/> : 
      <View style={styles.container}>
        <Content>
          {/* INFORMACION PERSONAL */}
          
          <Text style={styles.title}>
            Por favor ingresa la información del nuevo usuario
          </Text>
          <Text style={styles.titleList}>INFORMACION PERSONAL</Text>
          <Item error={trySubmit && typeDocument.length===0} >
            <Form style={styles.pickerContainer} >
              <Text style={styles.textPicker}>Tipo de identificación: </Text>
              <Picker
                mode="dropdown"
                placeholder="  "
                placeholderStyle={{ color: "#2874F0" }}
                note={false}
                selectedValue={typeDocument}
                onValueChange={handleTypeDocument}
                error={trySubmit} 
                // style={styles.typeDocumentPicker}
              >
                <Picker.Item label="Cédula de ciudadanía" value="CC" textStyle={{ color: "blue" }} />
                <Picker.Item label="Pasaporte" value="PA" />
                <Picker.Item label="Cédula de extranjería" value="CE" />
                <Picker.Item label="Tarjeta de identidad" value="TI" />
              </Picker>
            </Form>
          </Item>
          <Item floatingLabel style={styles.input} error={trySubmit && idUser.length===0} success={trySubmit && idUser.length>0}>
            <Label>Número de identificación</Label>
            <Input
              onChangeText={(e)=>{setidUser(e)}}
              keyboardType='numeric'
              // value={idUser}
            />
          </Item>
          <Item floatingLabel style={styles.input} error={trySubmit && nameNewUser.length===0} success={trySubmit && nameNewUser.length>0}>
            <Label>Nombre</Label>
            <Input
              onChangeText={(e)=>{setNameNewUser(e)}}
              keyboardType='default'
              // value={idUser}
            />
          </Item>
          <Item floatingLabel style={styles.input} error={trySubmit && lastName.length===0} success={trySubmit && lastName.length>0}>
            <Label>Apellido</Label>
            <Input
              onChangeText={(e)=>{setLastName(e)}}
              keyboardType='default'
              // value={idUser}
            />
          </Item>
          <Item floatingLabel style={styles.input} error={trySubmit && email.length===0} success={trySubmit && email.length>0}>
            <Label>Correo electrónico</Label>
            <Input
              onChangeText={(e)=>{setEmail(e)}}
              keyboardType='email-address'
              // value={idUser}
            />
          </Item>
          {/* INFORMACION GENERAL */}
          <Text style={styles.titleList}>INFORMACION GENERAL</Text>
          <Item style={styles.pickerContainer} error={trySubmit && email.length===0} success={trySubmit && fechaNacimiento.length>0}>
            <Text style={styles.textPicker}>Fecha de nacimiento: </Text>
            {/* {fechaNacimiento.toString().substr(4, 12)}*/}
            <View style={{ paddingTop: 6, marginLeft:25}}>
              <DatePicker 
                defaultDate={new Date()}
                minimumDate={new Date(1920, 1, 1)}
                maximumDate={new Date()}
                locale={"es"}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                // placeHolderText={"19/05/2020"}
                textStyle={{ color: "black", fontWeight:'bold', fonstSize:18 }}
                placeHolderTextStyle={{ color: "black" }}
                onDateChange={sethandleDatebirthday}
                disabled={false}
              />
            </View>
          </Item>
          <Item error={trySubmit && sexo.length===0} success={trySubmit && sexo.length>0}>
            <Form style={styles.pickerContainer}>
              <Text style={styles.textPicker}>Sexo: </Text>
              <Picker
                mode="dropdown"
                placeholder="  "
                placeholderStyle={{ color: "#2874F0" }}
                note={false}
                selectedValue={sexo}
                onValueChange={handleSetSexo}
                // style={styles.typeDocumentPicker}
              >
                <Picker.Item label="Masculino" value="M"  />
                <Picker.Item label="Femenino" value="F" />
              </Picker>
            </Form>
          </Item>
          <Item error={trySubmit && tipoSangre.length===0} success={trySubmit && tipoSangre.length>0} >
            <Form style={styles.pickerContainer}>
              <Text style={styles.textPicker}>Tipo de Sangre: </Text>
              <Picker
                mode="dropdown"
                placeholder="  "
                placeholderStyle={{ color: "#2874F0" }}
                note={false}
                selectedValue={tipoSangre}
                onValueChange={handleSetTipoSangre}
              >
                <Picker.Item label="O+" value="O+" />
                <Picker.Item label="O-" value="O-" />
                <Picker.Item label="A+" value="A+" />
                <Picker.Item label="A-" value="A-" />
                <Picker.Item label="B+" value="B+" />
                <Picker.Item label="B-" value="B-" />
                <Picker.Item label="AB+" value="AB+" />
                <Picker.Item label="AB-" value="AB-" />
              </Picker>
            </Form>
          </Item>
          <Item error={trySubmit && estadoCivil.length===0} success={trySubmit && estadoCivil.length>0} >
            <Form style={styles.pickerContainer}>
              <Text style={styles.textPicker}>Estado civíl: </Text>
              <Picker
                mode="dropdown"
                placeholder="  "
                placeholderStyle={{ color: "#2874F0" }}
                note={false}
                selectedValue={estadoCivil}
                onValueChange={handleSetEstadoCivil}
                // style={styles.typeDocumentPicker}
              >
                <Picker.Item label="Solter@" value="Solter@" />
                <Picker.Item label="Casad@" value="Casad@" />
                <Picker.Item label="Divercid@" value="Divercid@" />
                <Picker.Item label="Unión libre" value="Union libre" />
              </Picker>
            </Form>
          </Item>
            <Item floatingLabel style={styles.input} error={trySubmit && eps.length===0} success={trySubmit && eps.length>0} >
              <Label>EPS/ARL</Label>
              <Input
                onChangeText={(e)=>{setEps(e)}}
                keyboardType='default'
                // value={idUser}
              />
            </Item>

          {/* INFORMACIÓN DE CONTACTO */}
          <Text style={styles.titleList}>INFORMACIÓN DE CONTACTO</Text>
            <Item floatingLabel style={styles.input} error={trySubmit && nombreContacto.length===0} success={trySubmit && nombreContacto.length>0}>
              <Label>Nombre de contacto</Label>
              <Input
                onChangeText={(e)=>{setNombreContacto(e)}}
                keyboardType='default'
                // value={idUser}
              />
            </Item>
            <Item floatingLabel style={styles.input} error={trySubmit && apellidoContacto.length===0} success={trySubmit && apellidoContacto.length>0}>
              <Label>Apellido de contacto</Label>
              <Input
                onChangeText={(e)=>{setApellidoContacto(e)}}
                keyboardType='default'
                // value={idUser}
              />
            </Item>
            <Item floatingLabel style={styles.input} error={trySubmit && telefonoContacto.length===0} success={trySubmit && telefonoContacto.length>0}>
              <Label>Telefono de contacto</Label>
              <Input
                onChangeText={(e)=>{setTelefonoContacto(e)}}
                keyboardType='numeric'
                // value={idUser}
              />
            </Item>
            <Item floatingLabel style={styles.input} error={trySubmit && relacionPersonaContacto.length===0} success={trySubmit && relacionPersonaContacto.length>0}>
              <Label>Relación de contacto</Label>
              <Input
                onChangeText={(e)=>{setrelacionPersonaContacto(e)}}
                keyboardType='default'
              />
            </Item>
          {/* DATOS DE UBICACION */}
          <Text style={styles.titleList}>DATOS DE UBICACIÓN</Text>
            <Item floatingLabel style={styles.input} error={trySubmit && telefonoCasa.length===0} success={trySubmit && telefonoCasa.length>0}>
              <Label>Telefono de casa</Label>
              <Input
                onChangeText={(e)=>{setTelefonoCasa(e)}}
                keyboardType='numeric'
              />
            </Item>
            <Item floatingLabel style={styles.input} error={trySubmit && celular.length===0} success={trySubmit && celular.length>0}>
              <Label>Telefono móvil</Label>
              <Input
                onChangeText={(e)=>{setCelular(e)}}
                keyboardType='numeric'
                // value={idUser}
              />
            </Item>
            <Item floatingLabel style={styles.input} error={trySubmit && telefonoOficina.length===0} success={trySubmit && telefonoOficina.length>0}>
              <Label>Telefono de oficina</Label>
              <Input
                onChangeText={(e)=>{setTelefonoOficina(e)}}
                keyboardType='numeric'
                // value={idUser}
              />
            </Item>
            <Item floatingLabel style={styles.input} error={trySubmit && direccion.length===0} success={trySubmit && direccion.length>0}>
              <Label>Dirección de residencia</Label>
              <Input
                onChangeText={(e)=>{setDireccion(e)}}
                keyboardType='default'
                // value={idUser}
              />
            </Item>
            <Item floatingLabel style={styles.input} error={trySubmit && ciudad.length===0} success={trySubmit && ciudad.length>0}>
              <Label>Ciudad</Label>
              <Input
                onChangeText={(e)=>{setCiudad(e)}}
                keyboardType='default'
                // value={idUser}
              />
            </Item>
            {/* INFORMACIÓN DE SESIÓN */}
          <Text style={styles.titleList}>INFORMACIÓN DE SESIÓN</Text>
            <Item floatingLabel style={styles.input}error={trySubmit && profesion.length===0} success={trySubmit && profesion.length>0}>
              <Label>Profesión</Label>
              <Input
                onChangeText={(e)=>{setProfesion(e)}}
                keyboardType='default'
                // value={idUser}
              />
            </Item>
            <Item error={trySubmit && rolUsuario.length===0} success={trySubmit && rolUsuario.length>0} >
              <Form style={styles.pickerContainer}>
                <Text style={styles.textPicker}>Roles de sesión: </Text>
                <Picker
                  mode="dropdown"
                  placeholder="  "
                  placeholderStyle={{ color: "#2874F0" }}
                  note={false}
                  selectedValue={rolUsuario}
                  onValueChange={handleSeRolUsuario}
                  // style={styles.typeDocumentPicker}
                >
                  <Picker.Item label="Admin" value='1' />
                  <Picker.Item label="Doctor" value='2' />
                  <Picker.Item label="paciente" value='3' />
                  <Picker.Item label="laboratorista" value='4' />
                </Picker>
              </Form>
            </Item>
            <Item floatingLabel style={styles.input} error={!!validationPassword && !!trySubmit} success={trySubmit && contrasena.length>0}>
              <Label>Contraseña</Label>
              <Input
                onChangeText={(e)=>{setContrasena(e)}}
                keyboardType='default'
                // value={idUser}
              />
            </Item>
            <Item floatingLabel style={styles.input} error={!!validationPassword && !!trySubmit} success={trySubmit && confirmContrasena.length>0}>
              <Label>Confirmar contraseña</Label>
              <Input
                onChangeText={(e)=>{setConfirmContrasena(e)}}
                keyboardType='default'
                // value={idUser}
              />
            </Item>
            {!!validationPassword && !!trySubmit ? <Text style={styles.passwordAlert}>Las contraseñas deben coincidir</Text> : null}
            <View style={{backgroundColor: 'white'}}>
              <View style={styles.btnContainer}>
                <TouchableHighlight 
                  style={styles.btnStyle}
                  onPress={handleSubmit}
                >
                  <Text style={styles.btnText}>Agregar</Text>
                </TouchableHighlight>
              </View>
            </View>
        </Content>
      </View>
      }
    </TouchableWithoutFeedback>
  );
};
const styles = StyleSheet.create({
  container:{
    flex: 1,
    // paddingTop:'5%'
  },
  title:{
    fontWeight:'bold',
    fontSize: 20,
    textAlign:'center',
    paddingVertical:'3%',
    backgroundColor:'white',
    borderBottomColor: 'black',
    borderBottomWidth: 2
  
  },
  titleList:{
    backgroundColor:'white',
    color:'black',
    fontWeight:'bold',
    paddingVertical: 20,
    paddingLeft:10,
    fontSize: 17
  },
  input:{
    marginLeft:10,
    marginBottom: 20,
  },
  alert:{
    color:'red',
    marginHorizontal:'5%'
  },
  passwordAlert:{
    color:'red',
    marginLeft: '4%'
  },
  pickerContainer:{
    flexDirection:'row',
    marginLeft: 0,
    paddingLeft: 10,
    borderBottomWidth:0.8,
    marginBottom: 10,
    borderBottomColor: '#d3d3d3'
  },
  textPicker:{
    fontSize: 17,
    paddingVertical:'3.5%'
  },
  btnContainer:{
    height: 30,
    textAlign: 'center',
    paddingHorizontal:'15%',
    marginVertical: 40,
    backgroundColor: 'white'
  },
  btnStyle:{
    backgroundColor: 'linear-gradient(to right, rgba(128,82,255,1) 0%, rgba(151,64,231,1) 100%)',
    flex: 1,
    justifyContent:'center',
    borderRadius: 10,
    borderColor: 'gray',
    borderWidth: 1
  },
  btnText:{
    textAlign:'center',
    color: 'white'
  }
});
export default AdministradorNewUserView;