import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AdminHomeView from './AdministradorHomeView';
import AdministradorNewUserView from './AdministradorNewUserView';

const AdministradorViewManager = () =>{
    const Stack = createStackNavigator();
    return(
        <Stack.Navigator>
            <Stack.Screen name="Home" component={AdminHomeView}/>
            <Stack.Screen name="NewUser" component={AdministradorNewUserView}/>
            {/*<Stack.Screen name="UpdateUser" component={UpdateUser}/> */}
        </Stack.Navigator>
    )
}
export default AdministradorViewManager;