import 'react-native-gesture-handler';
import React,{useState} from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback, Keyboard, TouchableHighlight } from 'react-native';
import { Item, Input, Label } from 'native-base';
import Loader from '../../components/Loader';

const AdminHomeView = ({ navigation }) => {
  const [turnOnLoader, setTurnOnLoader]= useState(false);
  const [userToUpdate, setUserToUpdate] = useState(''); 

  //Validacion id
  const [idUserValidation, setidUserValidation] = useState(false);
  const withOutKeyboardFuntion = () =>{
    Keyboard.dismiss();
  }

  const handleUpdateUser = ()=>{
    if(userToUpdate === '') {setidUserValidation(true); return}
    else{setidUserValidation(false)}
     //Aquí se ahce el llamado a la consulta url
     //url=localhost.../id/{user}, se hace un async await
    setTurnOnLoader(true);
    setTimeout(() => {
      console.log('Enviando data a Update View, con id:', userToUpdate );
      setTurnOnLoader(false);
      const responseService= {
        status:200,
        data:{
          rol: 1,
          firstName: 'Jesus',
          secondName: 'Alexis',
          lastName: 'Canon',
          secondLastName: 'Andrade'
        }
      };
      navigation.navigate('UpdateUser', userToUpdate);
    }, 2000);
  }

  const handleAddUser = ()=>{
    navigation.navigate('NewUser');
  }

  return (
    <TouchableWithoutFeedback onPress={withOutKeyboardFuntion}>
      {turnOnLoader? <Loader/> : 
      <View style={styles.container}>
        <View ><Text style={{textAlign:'center'}}>Aqui va el Icono</Text></View>
        <Text style={styles.initialText}>Realiza acciones en los usuarios</Text>
        <View style={styles.form}>
          <View style={styles.btnContainer}>
            <TouchableHighlight 
              style={styles.btnStyle}
              onPress={handleAddUser}
            >
              <Text style={styles.btnText1}>+ Agregar usuario</Text>
            </TouchableHighlight>
          </View>
          <Item floatingLabel last style={styles.input}>
            <Label>Número de cédula</Label>
            <Input 
              onChangeText={(e)=>{setUserToUpdate(e)}}
              keyboardType='numeric'
            />
          </Item>
          {idUserValidation ? <Text style={styles.alert}>Campo requerido</Text> : null}
          <View style={styles.btnUpdateContainer}>
                <TouchableHighlight 
                    style={styles.btnStyle}
                    onPress={handleUpdateUser}
                >
                    <Text style={styles.btnText}>Actuazlizar/Eliminar usuario</Text>
                </TouchableHighlight>
                
            </View>
        </View>
      </View>
      }
    </TouchableWithoutFeedback>
  );
};
const styles = StyleSheet.create({
  container:{
    flex: 1,
    paddingTop:'10%'
  },
  form:{
    paddingHorizontal:'10%'
  },
  initialText:{
    fontWeight:'bold',
    fontSize: 20,
    textAlign:'center'
  },
  input:{
    marginTop: 40,
    marginHorizontal:'20%'
  },
  btnContainer:{
    height: 35,
    textAlign: 'center',
    paddingHorizontal:'15%',
    marginTop: 40
  },
  btnUpdateContainer:{
    height: 35,
    textAlign: 'center',
    paddingHorizontal:'15%',
    marginTop: 10
  },
  btnStyle:{
    backgroundColor: 'linear-gradient(to right, rgba(128,82,255,1) 0%, rgba(151,64,231,1) 100%)',
    flex: 1,
    justifyContent:'center',
    borderRadius: 10,
    borderColor: 'gray',
    borderWidth: 1
  },
  btnText:{
    textAlign:'center',
    color: 'white'
  },
  btnText1:{
    textAlign:'center',
    color: 'white',
    fontWeight:'bold'
  },
  alert:{
    color:'red',
    marginHorizontal:'5%'
  }
});
export default AdminHomeView;