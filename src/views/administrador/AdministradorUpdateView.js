import 'react-native-gesture-handler';
import React,{useState, useEffect} from 'react';
import { View, StyleSheet, TouchableWithoutFeedback, Keyboard, TouchableHighlight } from 'react-native';
import { Item, Container, Content, List, ListItem, Text } from 'native-base';
import Loader from '../../components/Loader';

const AdminUpdateView = ({ route, navigation }) => {
  // console.log("route.params: ", route.params);
  const [turnOnLoader, setTurnOnLoader]= useState(false);
  const [dataUserAlready, setDataUserAlready] = useState({});

  const withOutKeyboardFuntion = () =>{
    Keyboard.dismiss();
  }
  // const dataUser = await axios.get ("https://randomuser.me/api/?page=1&results=10&nat=us");
  useEffect (async () => { 
    setTurnOnLoader(true);
    const timer = setTimeout(() => {
      console.log("route.params: ", route.params);
      setTurnOnLoader(false);
      const responseService= {
        status:200,
        data:{
          ndi: 'CC',
          ndi_value: '1075265347',
          given: "nameNewUser",
          family: "lastName",
          email: "email",
          birthDate: "fechaNacimiento",
          gender: "sexo",
          bloodtype:"tipoSangre",
          maritalStatus: "estadoCivil",
          managingOrganization: "eps",
          relationship: "relacionPersonaContacto",
          givenc: "nombreContacto",
          familyc: "apellidoContacto",
          telc: "telefonoContacto",
          telhome: "telefonoCasa",
          telmobile: "celular",
          telwork: "telefonoOficina",
          line: "direccion",
          city: "ciudad",
          practitionerRole: "profesion"
        }
    };
    setDataUserAlready(responseService.data);
    }, 2000); 
    timer
  }, []);
  
  return (
    <TouchableWithoutFeedback onPress={withOutKeyboardFuntion}>
      {turnOnLoader? <Loader/> :
        <Container>
          <Content>
            <List>
              <ListItem itemDivider>
                <Text style={styles.title}>Información personal</Text>
              </ListItem>                    
              <ListItem>
                <View>
                  <Text style={styles.titleData}>Número de identificación: </Text>
                  <Text style={styles.infoData}>{" "+ dataUserAlready.ndi_value}</Text>
                </View>
              </ListItem>
              <ListItem>
                <View>
                  <Text style={styles.titleData}>Nombre: </Text>
                  <Text style={styles.infoData}>{dataUserAlready.given + ' '+ dataUserAlready.family}</Text>
                </View>
              </ListItem>
              <ListItem>
                <View>
                  <Text style={styles.titleData}>Correo electrónico: </Text>
                  <Text style={styles.infoData}>{" "+dataUserAlready.email}</Text>
                </View>
              </ListItem>
              <ListItem>
                <View>
                  <Text style={styles.titleData}>Tipo de identificación: </Text>
                  <Text style={styles.infoData}>{dataUserAlready.given + ' '+ dataUserAlready.family}</Text>
                </View>
              </ListItem>
              <ListItem>
                <Text>Ali Connors</Text>
              </ListItem>
              <ListItem itemDivider>
                <Text>Información de contacto</Text>
              </ListItem>  
              <ListItem>
                <Text>Bradley Horowitz</Text>
              </ListItem>
            </List>
          </Content>
      </Container>
    }
    </TouchableWithoutFeedback>
  );
};
const styles = StyleSheet.create({
  container:{
    flex: 1,
    paddingTop:'10%'
  },
  form:{
    paddingHorizontal:'10%'
  },
  initialText:{
    fontWeight:'bold',
    fontSize: 20,
    textAlign:'center'
  },
  input:{
    marginTop: 40,
    marginHorizontal:'20%'
  },
  btnContainer:{
    height: 35,
    textAlign: 'center',
    paddingHorizontal:'15%',
    marginTop: 40
  },
  btnUpdateContainer:{
    height: 35,
    textAlign: 'center',
    paddingHorizontal:'15%',
    marginTop: 10
  },
  btnStyle:{
    backgroundColor: 'linear-gradient(to right, rgba(128,82,255,1) 0%, rgba(151,64,231,1) 100%)',
    flex: 1,
    justifyContent:'center',
    borderRadius: 10,
    borderColor: 'gray',
    borderWidth: 1
  },
  btnText:{
    textAlign:'center',
    color: 'white'
  },
  btnText1:{
    textAlign:'center',
    color: 'white',
    fontWeight:'bold'
  },
  alert:{
    color:'red',
    marginHorizontal:'5%'
  }
});
export default AdminUpdateView;