import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import ViewRoutes from './src/viewNavigation';

const App = () => {
  return (
    <NavigationContainer>
      <ViewRoutes/>
    </NavigationContainer>
  );
};

export default App;
